﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor.AnimatedValues;
using UnityEditor;
using UnityEditor.UI;
using UnityExtensions.UI;

namespace UnityEditorExtensions.UI
{
    /// <summary>
    /// Editor class used to edit UI Sprites.
    /// </summary>

    [CustomEditor(typeof(AdvancedImage), true)]
    [CanEditMultipleObjects]
    public class AdvancedImageEditor : GraphicEditor
    {
        SerializedProperty m_FillMethod;
        SerializedProperty m_FillOrigin;
        SerializedProperty m_FillAmount;
        SerializedProperty m_FillClockwise;
        SerializedProperty m_Type;
        SerializedProperty m_FillCenter;
        SerializedProperty m_Sprite;
        SerializedProperty m_PreserveAspect;
        SerializedProperty m_FlipX;
        SerializedProperty m_FlipY;
        GUIContent m_SpriteContent;
        GUIContent m_SpriteTypeContent;
        GUIContent m_ClockwiseContent;
        AnimBool m_ShowSlicedOrTiled;
        AnimBool m_ShowSliced;
        AnimBool m_ShowFilled;
        AnimBool m_ShowType;
        AnimBool m_ShowFlip;

        protected override void OnEnable()
        {
            base.OnEnable();

            m_SpriteContent = new GUIContent("Source Image");
            m_SpriteTypeContent = new GUIContent("Image Type");
            m_ClockwiseContent = new GUIContent("Clockwise");

            m_Sprite = serializedObject.FindProperty("m_Sprite");
            m_Type = serializedObject.FindProperty("m_Type");
            m_FillCenter = serializedObject.FindProperty("m_FillCenter");
            m_FillMethod = serializedObject.FindProperty("m_FillMethod");
            m_FillOrigin = serializedObject.FindProperty("m_FillOrigin");
            m_FillClockwise = serializedObject.FindProperty("m_FillClockwise");
            m_FillAmount = serializedObject.FindProperty("m_FillAmount");
            m_PreserveAspect = serializedObject.FindProperty("m_PreserveAspect");
            m_FlipX = serializedObject.FindProperty("m_FlipX");
            m_FlipY = serializedObject.FindProperty("m_FlipY");

            m_ShowType = new AnimBool(m_Sprite.objectReferenceValue != null);
            m_ShowType.valueChanged.AddListener(Repaint);

            var typeEnum = (AdvancedImage.Type)m_Type.enumValueIndex;

            m_ShowSlicedOrTiled = new AnimBool(!m_Type.hasMultipleDifferentValues && typeEnum == AdvancedImage.Type.Sliced);
            m_ShowSliced = new AnimBool(!m_Type.hasMultipleDifferentValues && typeEnum == AdvancedImage.Type.Sliced);
            m_ShowFilled = new AnimBool(!m_Type.hasMultipleDifferentValues && typeEnum == AdvancedImage.Type.Filled);
            m_ShowFlip = new AnimBool(!m_Type.hasMultipleDifferentValues && (typeEnum == AdvancedImage.Type.Meshed || typeEnum == AdvancedImage.Type.Simple || typeEnum == AdvancedImage.Type.Sliced));

            m_ShowSlicedOrTiled.valueChanged.AddListener(Repaint);
            m_ShowSliced.valueChanged.AddListener(Repaint);
            m_ShowFilled.valueChanged.AddListener(Repaint);
            m_ShowFlip.valueChanged.AddListener(Repaint);

            SetShowNativeSize(true);
        }

        protected override void OnDisable()
        {
            m_ShowType.valueChanged.RemoveListener(Repaint);
            m_ShowSlicedOrTiled.valueChanged.RemoveListener(Repaint);
            m_ShowSliced.valueChanged.RemoveListener(Repaint);
            m_ShowFilled.valueChanged.RemoveListener(Repaint);
            m_ShowFlip.valueChanged.RemoveListener(Repaint);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            SpriteGUI();
            AppearanceControlsGUI();
            RaycastControlsGUI();

            m_ShowType.target = m_Sprite.objectReferenceValue != null;
            if (EditorGUILayout.BeginFadeGroup(m_ShowType.faded))
                TypeGUI();
            EditorGUILayout.EndFadeGroup();

            SetShowNativeSize(false);
            if (EditorGUILayout.BeginFadeGroup(m_ShowNativeSize.faded))
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(m_PreserveAspect);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.EndFadeGroup();
            NativeSizeButtonGUI();

            serializedObject.ApplyModifiedProperties();
        }

        void SetShowNativeSize(bool instant)
        {
            AdvancedImage.Type type = (AdvancedImage.Type)m_Type.enumValueIndex;
            bool showNativeSize = (type == AdvancedImage.Type.Simple || type == AdvancedImage.Type.Filled || type == AdvancedImage.Type.Meshed);
            base.SetShowNativeSize(showNativeSize, instant);
        }

        /// <summary>
        /// Draw the atlas and AdvancedImage selection fields.
        /// </summary>

        protected void SpriteGUI()
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(m_Sprite, m_SpriteContent);
            if (EditorGUI.EndChangeCheck())
            {
                var newSprite = m_Sprite.objectReferenceValue as Sprite;
                if (newSprite)
                {
                    AdvancedImage.Type oldType = (AdvancedImage.Type)m_Type.enumValueIndex;
                    if (newSprite.border.SqrMagnitude() > 0)
                    {
                        m_Type.enumValueIndex = (int)AdvancedImage.Type.Sliced;
                    }
                    else if (oldType == AdvancedImage.Type.Sliced)
                    {
                        m_Type.enumValueIndex = (int)AdvancedImage.Type.Simple;
                    }
                }
            }
        }

        /// <summary>
        /// Sprites's custom properties based on the type.
        /// </summary>

        protected void TypeGUI()
        {
            EditorGUILayout.PropertyField(m_Type, m_SpriteTypeContent);

            ++EditorGUI.indentLevel;
            {
                AdvancedImage.Type typeEnum = (AdvancedImage.Type)m_Type.enumValueIndex;

                bool showSlicedOrTiled = (!m_Type.hasMultipleDifferentValues && (typeEnum == AdvancedImage.Type.Sliced || typeEnum == AdvancedImage.Type.Tiled));
                if (showSlicedOrTiled && targets.Length > 1)
                    showSlicedOrTiled = targets.Select(obj => obj as AdvancedImage).All(img => img.hasBorder);

                m_ShowSlicedOrTiled.target = showSlicedOrTiled;
                m_ShowSliced.target = (showSlicedOrTiled && !m_Type.hasMultipleDifferentValues && typeEnum == AdvancedImage.Type.Sliced);
                m_ShowFilled.target = (!m_Type.hasMultipleDifferentValues && typeEnum == AdvancedImage.Type.Filled);
                m_ShowFlip.target = (!m_Type.hasMultipleDifferentValues && (typeEnum == AdvancedImage.Type.Meshed || typeEnum == AdvancedImage.Type.Simple || typeEnum == AdvancedImage.Type.Sliced));

                AdvancedImage image = target as AdvancedImage;

                if (EditorGUILayout.BeginFadeGroup(m_ShowFlip.faded))
                {
                    EditorGUILayout.PropertyField(m_FlipX);
                    EditorGUILayout.PropertyField(m_FlipY);
                }
                EditorGUILayout.EndFadeGroup();

                if (EditorGUILayout.BeginFadeGroup(m_ShowSlicedOrTiled.faded))
                {
                    if (image.hasBorder)
                        EditorGUILayout.PropertyField(m_FillCenter);
                }
                EditorGUILayout.EndFadeGroup();

                if (EditorGUILayout.BeginFadeGroup(m_ShowSliced.faded))
                {
                    if (image.sprite != null && !image.hasBorder)
                        EditorGUILayout.HelpBox("This Image doesn't have a border.", MessageType.Warning);
                }
                EditorGUILayout.EndFadeGroup();

                if (EditorGUILayout.BeginFadeGroup(m_ShowFilled.faded))
                {
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.PropertyField(m_FillMethod);
                    if (EditorGUI.EndChangeCheck())
                    {
                        m_FillOrigin.intValue = 0;
                    }
                    switch ((AdvancedImage.FillMethod)m_FillMethod.enumValueIndex)
                    {
                        case AdvancedImage.FillMethod.Horizontal:
                            m_FillOrigin.intValue = (int)(AdvancedImage.OriginHorizontal)EditorGUILayout.EnumPopup("Fill Origin", (AdvancedImage.OriginHorizontal)m_FillOrigin.intValue);
                            break;
                        case AdvancedImage.FillMethod.Vertical:
                            m_FillOrigin.intValue = (int)(AdvancedImage.OriginVertical)EditorGUILayout.EnumPopup("Fill Origin", (AdvancedImage.OriginVertical)m_FillOrigin.intValue);
                            break;
                        case AdvancedImage.FillMethod.Radial90:
                            m_FillOrigin.intValue = (int)(AdvancedImage.Origin90)EditorGUILayout.EnumPopup("Fill Origin", (AdvancedImage.Origin90)m_FillOrigin.intValue);
                            break;
                        case AdvancedImage.FillMethod.Radial180:
                            m_FillOrigin.intValue = (int)(AdvancedImage.Origin180)EditorGUILayout.EnumPopup("Fill Origin", (AdvancedImage.Origin180)m_FillOrigin.intValue);
                            break;
                        case AdvancedImage.FillMethod.Radial360:
                            m_FillOrigin.intValue = (int)(AdvancedImage.Origin360)EditorGUILayout.EnumPopup("Fill Origin", (AdvancedImage.Origin360)m_FillOrigin.intValue);
                            break;
                    }
                    EditorGUILayout.PropertyField(m_FillAmount);
                    if ((AdvancedImage.FillMethod)m_FillMethod.enumValueIndex > AdvancedImage.FillMethod.Vertical)
                    {
                        EditorGUILayout.PropertyField(m_FillClockwise, m_ClockwiseContent);
                    }
                }
                EditorGUILayout.EndFadeGroup();
            }
            --EditorGUI.indentLevel;
        }

        /// <summary>
        /// All graphics have a preview.
        /// </summary>

        public override bool HasPreviewGUI() { return true; }

        /// <summary>
        /// Draw the AdvancedImage preview.
        /// </summary>

        public override void OnPreviewGUI(Rect rect, GUIStyle background)
        {
            AdvancedImage image = target as AdvancedImage;
            if (image == null) return;

            Sprite sf = image.sprite;
            if (sf == null) return;

            SpriteDrawUtility.DrawSprite(sf, rect, image.canvasRenderer.GetColor());
        }

        /// <summary>
        /// Info String drawn at the bottom of the Preview
        /// </summary>

        public override string GetInfoString()
        {
            AdvancedImage image = target as AdvancedImage;
            Sprite sprite = image.sprite;

            int x = (sprite != null) ? Mathf.RoundToInt(sprite.rect.width) : 0;
            int y = (sprite != null) ? Mathf.RoundToInt(sprite.rect.height) : 0;

            return string.Format("Image Size: {0}x{1}", x, y);
        }
    }
}