﻿
using UnityEngine;

namespace UnityExtensions.UI
{
    public static class RectTransformExtensions
    {
        private static Vector3[] m_Corners = new Vector3[4];

        public static Rect GetWorldRect(this RectTransform rt)
        {
            if (rt == null)
                return new Rect();

            var vMin = new Vector2(float.MaxValue, float.MaxValue);
            var vMax = new Vector2(float.MinValue, float.MinValue);

            rt.GetWorldCorners(m_Corners);
            for (int i = 0; i < 4; i++)
            {
                vMin = Vector3.Min(m_Corners[i], vMin);
                vMax = Vector3.Max(m_Corners[i], vMax);
            }

            return Rect.MinMaxRect(vMin.x, vMin.y, vMax.x, vMax.y);
        }
    }
}
