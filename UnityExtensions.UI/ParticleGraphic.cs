﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using System.Collections.Generic;
using UnityEngine.Rendering;

namespace UnityExtensions.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasRenderer))]
    [RequireComponent(typeof(ParticleSystem))]
    [AddComponentMenu("UI/Particle Graphic", 15)]
    public class ParticleGraphic : MaskableGraphic
    {
        public struct ParticleState
        {
            public Color color;
            public Vector2 position;
            public float size;
        }

        public struct TextureSheetAnimation
        {
            public TextureSheetAnimation(ParticleSystem.TextureSheetAnimationModule mod)
            {
                animation = mod.animation;
                cycleCount = mod.cycleCount;
                enabled = mod.enabled;
                flipU = mod.flipU;
                flipV = mod.flipV;
                frameOverTime = mod.frameOverTime;
                frameOverTimeMultiplier = mod.frameOverTimeMultiplier;
                numTilesX = mod.numTilesX;
                numTilesY = mod.numTilesY;
                rowIndex = mod.rowIndex;
                startFrame = mod.startFrame;
                startFrameMultiplier = mod.startFrameMultiplier;
                useRandomRow = mod.useRandomRow;
                uvChannelMask = mod.uvChannelMask;
            }

            public ParticleSystemAnimationType animation;
            public int cycleCount;
            public bool enabled;
            public float flipU;
            public float flipV;
            public ParticleSystem.MinMaxCurve frameOverTime;
            public float frameOverTimeMultiplier;
            public int numTilesX;
            public int numTilesY;
            public int rowIndex;
            public ParticleSystem.MinMaxCurve startFrame;
            public float startFrameMultiplier;
            public bool useRandomRow;
            public UVChannelFlags uvChannelMask;
        }

        [FormerlySerializedAs("particleSprite")]
        public Sprite m_OverrideSprite;

        private Transform m_Transform;
        private ParticleSystem m_PS;
        private ParticleSystem.Particle[] m_Particles;
        private ParticleState[] m_ParticleStates;
        private Vector4 m_UV = Vector4.zero;
        private TextureSheetAnimation m_SheetAnimation;
        private float m_AnimationFrames;
        private Vector2 m_AnimationFrameSize;
        private float m_DeltaTime = 0f;
        private int m_ParticleCount = 0;
        private List<UIVertex> m_RenderVertices = null;
        private List<UIVertex> m_UpdateVertices = null;

        protected ParticleGraphic()
        {
            useLegacyMeshGeneration = false;
        }

        public override Texture mainTexture
        {
            get
            {
                if (m_OverrideSprite)
                    return m_OverrideSprite.texture;
                else if (m_Material)
                    return m_Material.mainTexture;
                else
                    return Texture2D.whiteTexture;
            }
        }

        protected bool Initialize()
        {
            // initialize members
            if (!m_Transform)
                m_Transform = GetComponent<Transform>();
            if (!m_PS)
            {
                m_PS = GetComponent<ParticleSystem>();
                ParticleSystemRenderer renderer = m_PS.GetComponent<ParticleSystemRenderer>();
                if (renderer)
                {
                    // don't draw anything, process by UIParticleSystem
                    renderer.renderMode = ParticleSystemRenderMode.None;
                }
            }
            if (m_Particles == null ||
                m_Particles.Length < m_PS.main.maxParticles)
            {
                m_Particles = new ParticleSystem.Particle[m_PS.main.maxParticles];
                m_ParticleStates = new ParticleState[m_PS.main.maxParticles];
            }
            if (m_OverrideSprite)
            {
                m_UV = UnityEngine.Sprites.DataUtility.GetOuterUV(m_OverrideSprite);
            }
            // prepare texture sheet animation
            m_SheetAnimation = new TextureSheetAnimation(m_PS.textureSheetAnimation);
            if (m_SheetAnimation.enabled)
            {
                m_AnimationFrames = m_SheetAnimation.numTilesX * m_SheetAnimation.numTilesY;
                m_AnimationFrameSize = new Vector2(1f / m_SheetAnimation.numTilesX, 1f / m_SheetAnimation.numTilesY);
            }
            else
            {
                m_AnimationFrames = 0f;
                m_AnimationFrameSize = Vector2.zero;
            }
            return true;
        }

        protected override void Awake()
        {
            base.Awake();
            if (!Initialize())
            {
                enabled = false;
            }
        }

        protected void RenderMesh(object state)
        {
            List<UIVertex> vl = state as List<UIVertex>;

            // iterate through current particles
            int neededCapacity = m_ParticleCount * 6;
            if (vl.Capacity < neededCapacity)
                vl.Capacity = neededCapacity;

            for (int i = 0; i < m_ParticleCount; ++i)
            {
                ParticleSystem.Particle particle = m_Particles[i];

                // get particle properties
                var prop = m_ParticleStates[i];

                // apply texture sheet animation
                Vector4 particleUV = m_UV;
                if (m_SheetAnimation.enabled)
                {
                    float particleTime = 1f - (particle.remainingLifetime / particle.startLifetime);
                    float lerpVector = (float)particle.randomSeed / uint.MaxValue;
                    float frameTime = (m_SheetAnimation.cycleCount * m_SheetAnimation.frameOverTime.Evaluate(particleTime, lerpVector)) % 1f;

                    int row;
                    int column;
                    switch (m_SheetAnimation.animation)
                    {
                        default: // NOTE: index need actually initialized
                        case ParticleSystemAnimationType.WholeSheet:
                            int index = Mathf.FloorToInt(m_AnimationFrames * frameTime);
                            column = index % m_SheetAnimation.numTilesX;
                            row = index / m_SheetAnimation.numTilesX;
                            break;

                        case ParticleSystemAnimationType.SingleRow:
                            row = m_SheetAnimation.useRandomRow ?
                                (int)(particle.randomSeed % m_SheetAnimation.numTilesY) : m_SheetAnimation.rowIndex;
                            column = Mathf.FloorToInt(Mathf.Lerp(0, m_SheetAnimation.numTilesX, frameTime));
                            break;
                    }

                    particleUV.x = column * m_AnimationFrameSize.x;
                    particleUV.w = 1f - (row * m_AnimationFrameSize.y);
                    particleUV.y = particleUV.w - m_AnimationFrameSize.y;
                    particleUV.z = particleUV.x + m_AnimationFrameSize.x;
                }

                var v0 = UIVertex.simpleVert;
                v0.color = prop.color;
                v0.uv0 = new Vector2(particleUV.x, particleUV.y);

                var v1 = UIVertex.simpleVert;
                v1.color = prop.color;
                v1.uv0 = new Vector2(particleUV.x, particleUV.w);

                var v2 = UIVertex.simpleVert;
                v2.color = prop.color;
                v2.uv0 = new Vector2(particleUV.z, particleUV.w);

                var v3 = UIVertex.simpleVert;
                v3.color = prop.color;
                v3.uv0 = new Vector2(particleUV.z, particleUV.y);

                if (particle.rotation == 0f)
                {
                    // no rotation
                    Vector2 corner1 = new Vector2(prop.position.x - prop.size, prop.position.y - prop.size);
                    Vector2 corner2 = new Vector2(prop.position.x + prop.size, prop.position.y + prop.size);

                    v0.position = new Vector2(corner1.x, corner1.y);
                    v1.position = new Vector2(corner1.x, corner2.y);
                    v2.position = new Vector2(corner2.x, corner2.y);
                    v3.position = new Vector2(corner2.x, corner1.y);
                }
                else
                {
                    float rotation = -particle.rotation * Mathf.Deg2Rad;
                    float rotation90 = rotation + Mathf.PI / 2;

                    // apply rotation
                    Vector2 dx = new Vector2(Mathf.Cos(rotation), Mathf.Sin(rotation)) * prop.size;
                    Vector2 dy = new Vector2(Mathf.Cos(rotation90), Mathf.Sin(rotation90)) * prop.size;

                    v0.position = prop.position - dx - dy;
                    v1.position = prop.position - dx + dy;
                    v2.position = prop.position + dx + dy;
                    v3.position = prop.position + dx - dy;
                }

                vl.Add(v0);
                vl.Add(v1);
                vl.Add(v2);
                vl.Add(v0);
                vl.Add(v2);
                vl.Add(v3);
            }
            m_UpdateVertices = vl;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            // prepare vertices
            vh.Clear();

            if (!gameObject.activeInHierarchy)
                return;

            if (m_RenderVertices != null)
            {
                vh.AddUIVertexTriangleStream(m_RenderVertices);
                ListPool<UIVertex>.Release(m_RenderVertices);
                m_RenderVertices = null;
            }
        }

        void Update()
        {
#if UNITY_EDITOR
        if (!Application.isPlaying)
            return;
#endif
            var _Canvas = canvas;
            if (_Canvas)
            {
                var rect = _Canvas.GetComponent<RectTransform>().GetWorldRect();
                if (!rect.Overlaps(rectTransform.GetWorldRect()))
                    return;

                UpdateParticle();
            }
        }

#if UNITY_EDITOR
    void LateUpdate()
    {
        if (!Application.isPlaying)
        {
            Initialize();
            UpdateParticle();
        }
    }

    protected override void OnValidate()
    {
        base.OnValidate();
        Initialize();
    }
#endif

        void UpdateParticleState()
        {
#if UNITY_EDITOR
        if (Application.isPlaying)
        {
            m_PS.Simulate(m_DeltaTime, false, false);
            m_DeltaTime = 0f;
        }
#else
            m_PS.Simulate(m_DeltaTime, false, false);
            m_DeltaTime = 0f;
#endif
            m_ParticleCount = m_PS.GetParticles(m_Particles);

            for (int i = 0; i < m_ParticleCount; ++i)
            {
                ParticleSystem.Particle particle = m_Particles[i];

                // get particle properties
                Vector2 position = m_PS.main.simulationSpace == ParticleSystemSimulationSpace.Local ?
                    particle.position :
                    m_Transform.InverseTransformPoint(particle.position);
                Color32 color = particle.GetCurrentColor(m_PS);
                float size = particle.GetCurrentSize(m_PS) * 0.5f;

                // apply scale
                if (m_PS.main.scalingMode == ParticleSystemScalingMode.Shape)
                {
                    position /= canvas.scaleFactor;
                }

                m_ParticleStates[i] = new ParticleState
                {
                    color = color,
                    position = position,
                    size = size,
                };
            }
        }

        void UpdateParticle()
        {
            m_DeltaTime += Time.unscaledDeltaTime;
            if (m_UpdateVertices != null)
            {
                m_RenderVertices = m_UpdateVertices;
                m_UpdateVertices = null;
                SetVerticesDirty();
            }
            if (m_UpdateVertices == null)
            {
                UpdateParticleState();
                System.Threading.ThreadPool.QueueUserWorkItem(RenderMesh, ListPool<UIVertex>.Get());
            }
        }
    }
}
