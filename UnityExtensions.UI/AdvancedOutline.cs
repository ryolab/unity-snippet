﻿
using UnityEngine;
using UnityEngine.UI;

namespace UnityExtensions.UI
{
    [AddComponentMenu("UI/Effects/Advanced Outline", 15)]
    public class AdvancedOutline : Outline
    {
        public enum Quality
        {
            Basic,
            Advanced,
        }

        [SerializeField]
        private Quality m_Quality;

        protected AdvancedOutline()
        {
        }

        public override void ModifyMesh(VertexHelper vh)
        {
            if (!IsActive())
                return;

            if (m_Quality == Quality.Basic)
            {
                base.ModifyMesh(vh);
            }
            else
            {
                var verts = ListPool<UIVertex>.Get();
                vh.GetUIVertexStream(verts);

                float dist = effectDistance.x;
                int piece = Mathf.RoundToInt(effectDistance.y);

                var neededCpacity = verts.Count * (piece + 1);

                if (verts.Capacity < neededCpacity)
                    verts.Capacity = neededCpacity;

                var start = 0;
                var end = verts.Count;
                ApplyShadowZeroAlloc(verts, effectColor, start, verts.Count, dist, 0);

                var unit = (2f * Mathf.PI) / piece;

                for (int i = 1; i < piece; ++i)
                {
                    var radian = i* unit;
                    Vector2 offset = new Vector2(Mathf.Sin(radian), Mathf.Cos(radian)) * dist;

                    start = end;
                    end = verts.Count;
                    ApplyShadowZeroAlloc(verts, effectColor, start, verts.Count, offset.x, offset.y);
                }

                vh.Clear();
                vh.AddUIVertexTriangleStream(verts);
                ListPool<UIVertex>.Release(verts);
            }
        }
    }
}