# unity-snippet

useful scripts snippet for unity

##Particle Graphic

Particle Graphic is extended from [Chan Calvin](https://www.linkedin.com/pulse/unity-ui-particle-system-chan-calvin/)'s implementation

####What's improved?

* more sheet animation module effect supported, such as frameOverTime, single-row mode ...
* compute particle vertices in thread-pool, reduce main thread usage
* started just like Chan Calvin's UI Particle System, remember you need to disable renderer module in ParticleSystem

##Advanced Outline

* extended from UnityEngine.UI.Outline
* default outline provide 4 times count of vertices to render effect, advanced Outline can set how much times you want
* Quality set to 'Basic' is provide Unity's default outline
* Quality set to 'Advanced' will change effectDistance's behaviour, X is distance to origin and Y is how much times you want to rendered (high to get better quality)
* recommanded for dynamic text

##Advanced Image

* extended from UnityEngine.UI.Image
* added options to flip image's horizontal by flipX and vertical by flipY (flip options are only support image type to Simple, Sliced and Meshed)
* added present type 'Meshed' which can drawing image with outline to prevent redundant drawing and make possible to use SpritePacker pack with policy 'TightRotateEnabledSpritePackerPolicy' and 'TightPackerPolicy' for Unity UI System.
